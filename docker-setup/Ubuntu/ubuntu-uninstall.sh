echo "Removing Docker repositories"
sudo add-apt-repository --remove "deb [arch=amd64] https://download.docker.com/linux/ubuntu hirsute stable"

# Updates apt once more
echo "Updating After removing the Dependices"
sudo apt update -y -qq

# Installs Docker
echo "Removing Docker"
sudo apt remove -y -qq docker-ce-*