#!/usr/bin/env bash
# A script for setting up post install
# Relies on Flatpak to be installed
# Created by Blake Ridgway

git submodule init
git submodule update --recursive

PACKAGE_LIST=(
  atop
  cargo
  curl
  build-essential
  dialog
  discord
  git
  gcc
  gnupg2
  slack-desktop
  htop
  libssl-dev 
  libncurses5-dev 
  libreadline-dev 
  libgdbm-dev 
  libdb5.3-dev 
  libbz2-dev 
  liblzma-dev 
  libsqlite3-dev 
  libffi-dev
  nano
  neofetch
  neovim
  network-manager-openvpn-gnome
  openjdk-11-jre
  python3
  python3-pip
  screen
  solaar
  steam
  system76-keyboard-configurator
  tilix
  tcl-dev 
  tk 
  tk-dev
  virt-manager
  vlc
  wget
  xonsh
  zlib1g-dev
  zoxide
  zsh
)

FLATPAK_LIST=(
  #com.github.fabiocolacio.marker
  #com.mattermost.Desktop
  #com.mojang.Minecraft
  com.obsproject.Studio
  net.veloren.Airshipper
  org.signal.Signal
  #org.telegram.Desktop
  com.anydesk.Anydesk
  com.getpostman.Postman
  io.github.Pithos
  md.obsidian.Obsidian
  org.xonotic.Xonotic
)

echo #######################
echo # Installing Packages #
echo #######################

# iterate through package and installs them
for package_name in ${PACKAGE_LIST[@]}; do
  if ! sudo apt list --installed | grep -q "^\<$package_name\>"; then
    echo "Installing $package_name..."
    sleep .5
    sudo apt install "$package_name" -y
    echo "$package_name has been installed"
  else
    echo "$package_name already installed"
  fi
done

for flatpak_name in ${FLATPAK_LIST[@]}; do
	if ! flatpak list | grep -q $flatpak_name; then
		flatpak install "$flatpak_name" -y
	else
		echo "$package_name already installed"
	fi
done


echo #####################
echo # Enable Proprietary codecs #
echo #####################
echo 
# https://support.system76.com/articles/codecs
sudo apt install -y ubuntu-restricted-extras

echo #####################
echo # Install Nerd Font #
echo #####################

# Nerd Font install
#wget https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/Hack/Regular/complete/Hack%20Regular%20Nerd%20Font%20Complete.ttf
#mkdir -p ~/.local/share/fonts 
#cp Hack\ Regular\ Nerd\ Font\ Complete.ttf ~/.local/share/fonts/
#fc-cache -f -v

# Firacode nerd font mono
font_url='https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraCode.zip'; font_name=${font_url##*/}; wget ${font_url} && unzip ${font_name} -d ~/.fonts && fc-cache -fv ;

sudo apt install -y fonts-firacode

echo ##################################
echo # Downloading and Configuring Go #
echo ##################################

# Grabs and downloads Go for Google
wget https://golang.org/dl/go1.16.4.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.16.4.linux-amd64.tar.gz

echo ###################
echo # Installing Rust #
echo ###################

# Install Rust
curl https://sh.rustup.rs -sSf | sh

echo ######################
echo # Setting up SSH Key #
echo ######################

# SSH Key Gen
ssh-keygen -t ed25519 -C ${USER}@$(hostname --fqdn)"

echo ######################
echo # Installing OhMyZSH #
echo ######################

# Oh-my-ZSH
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo ############################
echo # Setting up Powerlevel10k #
echo ############################

# POWERLEVEL10K
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

echo ##################################
echo # Copying p10k Config file to ~/
echo ##################################

# Copy p10k Config file
cp .p10k.zsh ~/


echo ##################################
echo # Install starship prompt
echo ##################################
sh -c "$(curl -fsSL https://starship.rs/install.sh)"

echo ##################################
echo # Install homebrew
echo ##################################
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

echo ##################################
echo # Install Pyenv
echo ##################################
curl https://pyenv.run | bash
python_version=3.9.6
CONFIGURE_OPTS=--enable-shared pyenv install $python_version && pyenv global $python_version && pyenv rehash

echo ##################################
echo # Install Keybase
echo ##################################
# https://keybase.io/docs/the_app/install_linux
curl --remote-name https://prerelease.keybase.io/keybase_amd64.deb
sudo apt install ./keybase_amd64.deb
run_keybase

echo ##################################
echo # Install Github CLI
echo ##################################
# https://github.com/cli/cli/blob/trunk/docs/install_linux.md
curl -fsSL https://cli.github.com/packages/githubcli-archive-keyring.gpg | sudo gpg --dearmor -o /usr/share/keyrings/githubcli-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/githubcli-archive-keyring.gpg] https://cli.github.com/packages stable main" | sudo tee /etc/apt/sources.list.d/github-cli.list > /dev/null
sudo apt update -y
sudo apt install -y gh

echo ##################################
echo # Install Matrix Element client : https://element.io/get-started
echo ##################################
sudo apt install -y wget apt-transport-https  
sudo wget -O /usr/share/keyrings/riot-im-archive-keyring.gpg https://packages.riot.im/debian/riot-im-archive-keyring.gpg  
echo "deb [signed-by=/usr/share/keyrings/riot-im-archive-keyring.gpg] https://packages.riot.im/debian/ default main" | sudo tee /etc/apt/sources.list.d/riot-im.list  
sudo apt update -y
sudo apt install element-desktop

echo ##################################
echo # Install Chrome
echo ##################################
curl --remote-name https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && sudo apt install -y ./google-chrome-stable_current_amd64.deb

echo ##################################
echo # Install Sentry
echo ##################################
# https://docs.sentry.io/product/cli/installation/
curl -sL https://sentry.io/get-cli/ | bash

echo ##################################
echo # Install FPM
echo ##################################
# https://fpm.readthedocs.io/en/latest/installing.html
sudo apt-get install ruby ruby-dev build-essential -y && sudo gem install --no-document fpm

echo ##################################
echo # Install Qt designer
echo ##################################
sudo apt-get install -y qtcreator pyqt5-dev-tools


echo ##################################
echo # Install virt-manager and osx prep
echo ##################################
sudo apt-get install qemu uml-utilities virt-manager git wget libguestfs-tools p7zip-full uml-utilities virt-viewer qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils -y
sudo virsh net-start default
sudo virsh net-autostart default

echo ##################################
echo # Install Docker and setup sudoless password for it
echo ##################################
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update -y
sudo apt install docker-ce docker-ce-cli containerd.io -y
sudo usermod -aG docker $USER

echo ##################################
echo # Install Argbash
echo ##################################
# https://argbash.readthedocs.io/en/latest/install.html
sudo apt-get install -y autoconf
wget https://github.com/matejak/argbash/archive/refs/tags/2.10.0.tar.gz
tar xvf 2.10.0.tar.gz
cd argbash-2.10.0/resources
make install PREFIX=$HOME/.local
echo # Argbash Installation completed

echo ##################################
echo # Install Yubikey needed tools
echo ##################################
# https://github.com/drduh/YubiKey-Guide
sudo apt -y install wget gnupg2 gnupg-agent dirmngr cryptsetup scdaemon pcscd secure-delete hopenpgp-tools yubikey-personalization
sudo apt -y install libssl-dev swig libpcsclite-dev
sudo apt -y install python3-pip python3-pyscard
pip3 install PyOpenSSL yubikey-manager
sudo service pcscd start

echo ############################
echo # Setting up nvim/init.vim #
echo ############################

mkdir -p ~/.config/nvim/
echo $'set runtimepath^=~/.vim runtimepath+=~/.vim/after\nlet &packpath=&runtimepath\nsource ~/.vimrc' > ~/.config/nvim/init.vim

echo ###############################
echo # Installing Android Messages #
echo ###############################

wget https://github.com/OrangeDrangon/android-messages-desktop/releases/download/v5.1.1/AndroidMessages-v5.1.1-linux-amd64.deb

sudo dpkg -i AndroidMessages-v5.1.1-linux-amd64.deb

echo #######################
echo # Cleanup and Updates #
echo #######################

sudo apt update
sudo apt upgrade -y
sudo apt autoremove -y
flatpak update

echo ################
echo # File Cleanup #
echo ################

rm -r *.ttf *.tar.gz

FILES=( 'vimrc' 'vim' 'zshrc' 'xonshrc' 'zsh' 'agignore' 'gitconfig' 'gitignore' 'gitmessage' )
for file in ${FILES[@]}; do
  echo ""
  echo "Simlinking $file to $HOME"
  ln -sf "$PWD/$file" "$HOME/.$file"
  if [ $? -eq 0 ]
  then
    echo "$PWD/$file ~> $HOME/.$file"
  else
    echo 'Install failed to symlink.'
    exit 1
  fi
done

